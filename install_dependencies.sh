#!/bin/bash

TERRAFORM_VERSION=1.0.6
KUBECTL_VERSION=1.21.0

sudo apt-get update --yes
sudo apt-get upgrade --yes
sudo apt-get install --yes curl wget unzip

curl -sS -o /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
    unzip /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /tmp/terraform && \
    sudo mv /tmp/terraform/terraform /usr/bin

sudo rm -rf /usr/bin/aws && \
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
unzip awscliv2.zip && \
sudo ./aws/install && \
sudo mv /usr/local/bin/aws /usr/bin

wget -q --show-progress --https-only --timestamping \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssl \
  https://storage.googleapis.com/kubernetes-the-hard-way/cfssl/1.4.1/linux/cfssljson && \
chmod +x cfssl cfssljson && \
sudo mv cfssl cfssljson /usr/local/bin/

wget https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
chmod +x kubectl && \
sudo mv kubectl /usr/local/bin/

rm -rf /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
rm -rf /tmp/terraform
rm -rf aws awscliv2.zip