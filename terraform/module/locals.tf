# #####
# locals {
#   pki_bucket_name = "${var.pki_bucket_name_prefix}-${formatdate("YYYYMMDDhhmmss", timestamp())}"
# }

locals {
  private_ip_1 = "10.0.0.1"
  private_ip_2 = "10.0.0.2"
  private_ip_3 = "10.0.0.3"
  lb_ip = "123.123.123.123"
}
