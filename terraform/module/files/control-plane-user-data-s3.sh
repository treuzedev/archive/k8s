#!/bin/bash

#####
# PREPARATIONS
#####

# variables that dont need awscli
export SLACK_WEBHOOK_URL="${slack_webhook_url}"
export BUCKET="${bucket}"
export PKI_FILES_DIR="${pki_files_dir}"
export KUBECONFIG_FILES_DIR="${kubeconfig_files_dir}"
export NUMBER_OF_PKI_FILES="${number_of_pki_files}"
export NUMBER_OF_KUBECONFIG_FILES="${number_of_kubeconfig_files}"
export ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
export C="${c}"
export L="${l}"
export APP_TAG_VALUE="${app_tag_value}"
export COMPONENT_TAG_VALUE="${component_tag_value}"
export ENVIRONMENT_TAG_VALUE="${environment_tag_value}"
export LB_PUBLIC_IP="${lb_public_ip}"
export CLUSTER_CIDR="${cluster_cidr}"

# TODO: what if there is more than one nat? why is an external ip needed? can we use the lb's ip?
# TODO: fail if there is no public ip / nat
# variables that need awscli
export NAT_GATEWAY_ID="${nat_gateway_id}"
export EXTERNAL_IP=$(aws ec2 describe-nat-gateways --nat-gateway-ids $NAT_GATEWAY_ID | jq -r '.NatGateways[0].NatGatewayAddresses[0].PublicIp')
export INTERNAL_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

echo "no step defined" > /tmp/step-bootstrap-controlplane


#####
# FUNCTIONS
#####

function curl_slack {
  set -euo pipefail
  STEP=$(cat /tmp/step-bootstrap-controlplane)
  curl \
    -s \
    -X POST \
    -H "Content-type: application/json" \
    -d "{
      \"text\": \"Error in function '$function' step '$STEP' with exit code '$ERROR'.\nInstance Id: '$ID'.\"
    }"\
    $SLACK_WEBHOOK_URL
  set +euo pipefail
}

function tag_instance {(
  set -euo pipefail
  echo "- tagging instance.."
  FLAG="false"
  NAME=""
  DATE=$(date +'%s')
  NAME_TAG="controlplane-$DATE"
  TAGS=$(aws ec2 describe-tags --filters "Name=resource-id,Values=$ID" | jq -c '.Tags[]')
  TAGS_ARRAY=($TAGS)
  for line in "$${TAGS_ARRAY[@]}"
  do
    KEY=$(echo $line | jq -r '.Key')
    if [[ $KEY == "Name" ]]
    then
      FLAG="true"
      NAME=$(echo $line | jq -r '.Value')
    fi
  done
  if [[ $FLAG == "false" ]]
  then
    aws ec2 create-tags --resources $ID --tags Key=Name,Value=$NAME_TAG
    echo "Instance tagged with 'Name=$NAME_TAG'."
  else
    echo "Instance already had a tag 'Name=$NAME'."
  fi
  set +euo pipefail
)}

function wait_for_files {(
  set -euo pipefail
  echo "- waiting for pki files to be in s3 bucket.."
  FLAG="false"
  while [[ "$FLAG" == "false" ]]
  do
    OBJECTS=$(aws s3 ls s3://$BUCKET/$PKI_FILES_DIR --recursive --summarize | grep 'Total Objects:' | awk '{print $NF}')
    if [[ "$OBJECTS" == $NUMBER_OF_PKI_FILES ]]
    then
      echo "files are ready!"
      FLAG="true"
    else
      echo "files not ready yet.."
      sleep 10
    fi
  done
  echo "- waiting for kubeconfig files to be in s3 bucket.."
  FLAG="false"
  while [[ "$FLAG" == "false" ]]
  do
    OBJECTS=$(aws s3 ls s3://$BUCKET/$KUBECONFIG_FILES_DIR --recursive --summarize | grep 'Total Objects:' | awk '{print $NF}')
    if [[ "$OBJECTS" == $NUMBER_OF_KUBECONFIG_FILES ]]
    then
      echo "files are ready!"
      FLAG="true"
    else
      echo "files not ready yet.."
      sleep 10
    fi
  done
  set +euo pipefail
)}

function get_pki_files {(
  set -euo pipefail
  echo "- retrieving pki files.."
  mkdir -p /usr/k8s-files/pki
  echo "retrieving ca-config.json"
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca-config.json /usr/k8s-files/pki
  echo "retrieving ca.pem and ca-key.pem"
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca.pem /usr/k8s-files/pki
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/ca-key.pem /usr/k8s-files/pki
  echo "retrieving kubernetes api server certificate and private key.."
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kubernetes-key.pem /usr/k8s-files/pki
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/kubernetes.pem /usr/k8s-files/pki
  echo "retrieving service account certificate and private key.."
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/service-account-key.pem /usr/k8s-files/pki
  aws s3 cp s3://$BUCKET/$PKI_FILES_DIR/service-account.pem /usr/k8s-files/pki
  echo "files retrieved:"
  ls /usr/k8s-files/pki
  echo "certificates ready!"
  set +euo pipefail
)}

function get_kubeconfig_files {(
  set -euo pipefail
  echo "- retrieving kubeconfig files.."
  mkdir -p /usr/k8s-files/kubeconfig
  aws s3 cp s3://$BUCKET/$KUBECONFIG_FILES_DIR/kube-controller-manager.kubeconfig /usr/k8s-files/kubeconfig
  aws s3 cp s3://$BUCKET/$KUBECONFIG_FILES_DIR/kube-scheduler.kubeconfig /usr/k8s-files/kubeconfig
  aws s3 cp s3://$BUCKET/$KUBECONFIG_FILES_DIR/admin.kubeconfig /usr/k8s-files/kubeconfig
  echo "files retrieved:"
  ls /usr/k8s-files/kubeconfig
  echo "kubeconfig files ready!"
  set +euo pipefail
)}

function bootstrap_etcd_cluster {(
  set -euo pipefail
  echo "creating etcd service"
  echo "downloading wget.." | tee /tmp/step-bootstrap-controlplane
  wget -q "https://github.com/etcd-io/etcd/releases/download/v3.4.15/etcd-v3.4.15-linux-amd64.tar.gz"
  tar -xvf etcd-v3.4.15-linux-amd64.tar.gz
  mv etcd-v3.4.15-linux-amd64/etcd* /usr/local/bin/
  echo "configuring etcd server" | tee /tmp/step-bootstrap-controlplane
  mkdir -p /etc/etcd /var/lib/etcd
  chmod 700 /var/lib/etcd
  cp /usr/k8s-files/pki/ca.pem /etc/etcd/
  cp /usr/k8s-files/pki/kubernetes-key.pem /etc/etcd/
  cp /usr/k8s-files/pki/kubernetes.pem /etc/etcd/
  cp /usr/k8s-files/service-files/etcd.service /etc/systemd/system/
  echo "etcd service file:"
  echo "---"
  cat /etc/systemd/system/etcd.service
  echo "---"
  echo "starting etcd server" | tee /tmp/step-bootstrap-controlplane
  systemctl daemon-reload
  systemctl enable etcd
  systemctl start etcd
  echo "veryfing etcd service" | tee /tmp/step-bootstrap-controlplane
  sleep 30
  ETCDCTL_API=3
  etcdctl member list \
    --endpoints=https://127.0.0.1:2379 \
    --cacert=/etc/etcd/ca.pem \
    --cert=/etc/etcd/kubernetes.pem \
    --key=/etc/etcd/kubernetes-key.pem
  echo "etcd service created successfully!"
  set +euo pipefail
)}

function get_controllers_internal_ips_for_etcd_service {(
  set -euo pipefail
  IPS=$(aws ec2 describe-instances \
    --filters "Name=tag:App,Values=$APP_TAG_VALUE" \
    --filters "Name=tag:Component,Values=$COMPONENT_TAG_VALUE" \
    --filters "Name=tag:Environment,Values=$ENVIRONMENT_TAG_VALUE" | jq -r '.Reservations[].Instances[0].PrivateIpAddress')
  IPS_ARRAY=($IPS)
  FLAG="first"
  TMP_INITIAL_CLUSTER=""
  for IP in "$${IPS_ARRAY[@]}"
  do
    if [[ "$IP" != "null" ]]
    then
      IP_ARR=($(echo $IP | tr '.' '\n'))
      NEW_IP="ip-$${IP_ARR[0]}-$${IP_ARR[1]}-$${IP_ARR[2]}-$${IP_ARR[3]}"
      TMP="$${NEW_IP}=https://$${IP}:2380"
      if [[ "$FLAG" == "first" ]]
      then
        TMP_INITIAL_CLUSTER="$TMP"
        FLAG="notfirst"
      else
        TMP_INITIAL_CLUSTER="$${TMP_INITIAL_CLUSTER},$${TMP}"
      fi
    fi
  done
  echo $TMP_INITIAL_CLUSTER
  set +euo pipefail
)}

function get_controllers_etcd_server_ips {(
  set -euo pipefail
  IPS=$(aws ec2 describe-instances \
    --filters "Name=tag:App,Values=$APP_TAG_VALUE" \
    --filters "Name=tag:Component,Values=$COMPONENT_TAG_VALUE" \
    --filters "Name=tag:Environment,Values=$ENVIRONMENT_TAG_VALUE" | jq -r '.Reservations[].Instances[0].PrivateIpAddress')
  IPS_ARRAY=($IPS)
  FLAG="first"
  TMP_ETCD_SERVERS=""
  for IP in "$${IPS_ARRAY[@]}"
  do
    if [[ "$IP" != "null" ]]
    then
      TMP="https://$${IP}:2379"
      if [[ "$FLAG" == "first" ]]
      then
        TMP_ETCD_SERVERS="$TMP"
        FLAG="notfirst"
      else
        TMP_ETCD_SERVERS="$${TMP_ETCD_SERVERS},$${TMP}"
      fi
    fi
  done
  echo $TMP_ETCD_SERVERS
  set +euo pipefail
)}

function bootstrap_k8s_controller {(
  set -euo pipefail
  echo "creating config dirs" | tee /tmp/step-bootstrap-controlplane
  mkdir -p /etc/kubernetes/config
  mkdir -p /var/lib/kubernetes/
  echo "copying necessary files" | tee /tmp/step-bootstrap-controlplane
  cp /usr/k8s-files/pki/ca.pem /var/lib/kubernetes/
  cp /usr/k8s-files/pki/ca-key.pem /var/lib/kubernetes/
  cp /usr/k8s-files/pki/kubernetes-key.pem /var/lib/kubernetes/
  cp /usr/k8s-files/pki/kubernetes.pem /var/lib/kubernetes/
  cp /usr/k8s-files/pki/service-account-key.pem /var/lib/kubernetes/
  cp /usr/k8s-files/pki/service-account.pem /var/lib/kubernetes/
  cp /usr/k8s-files/others/encryption-config.yaml /var/lib/kubernetes/
  cp /usr/k8s-files/others/kube-scheduler.yaml /etc/kubernetes/config/
  cp /usr/k8s-files/service-files/kube-apiserver.service /etc/systemd/system/
  cp /usr/k8s-files/service-files/kube-controller-manager.service /etc/systemd/system/
  cp /usr/k8s-files/service-files/kube-scheduler.service /etc/systemd/system/
  cp /usr/k8s-files/kubeconfig/kube-controller-manager.kubeconfig /var/lib/kubernetes/
  cp /usr/k8s-files/kubeconfig/kube-scheduler.kubeconfig /var/lib/kubernetes/
  echo "installing controller components" | tee /tmp/step-bootstrap-controlplane
  install_k8s_controller_components
  echo "starting services" | tee /tmp/step-bootstrap-controlplane
  start_k8s_controller_services
  set +euo pipefail
)}

function install_k8s_controller_components {(
  set -euo pipefail
  if [[ ! -f "/usr/local/bin/kube-apiserver" ]]
  then
    echo "installing kube-apiserver" | tee /tmp/step-bootstrap-controlplane
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kube-apiserver"
    chmod +x kube-apiserver
    mv kube-apiserver /usr/local/bin/
  else
    echo "kube-apiserver already installed" | tee /tmp/step-bootstrap-controlplane
  fi
  if [[ ! -f "/usr/local/bin/kube-controller-manager" ]]
  then
    echo "installing controller-manager" | tee /tmp/step-bootstrap-controlplane
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kube-controller-manager"
    chmod +x kube-controller-manager
    mv kube-controller-manager /usr/local/bin/
  else
    echo "kube-controller-manager already installed" | tee /tmp/step-bootstrap-controlplane
  fi
  if [[ ! -f "/usr/local/bin/kube-scheduler" ]]
  then
    echo "installing kube-scheduler" | tee /tmp/step-bootstrap-controlplane
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kube-scheduler"
    chmod +x kube-scheduler
    mv kube-scheduler /usr/local/bin/
  else
    echo "kube-scheduler already installed" | tee /tmp/step-bootstrap-controlplane
  fi
  if [[ ! -f "/usr/local/bin/kubectl" ]]
  then
    echo "installing kubectl" | tee /tmp/step-bootstrap-controlplane
    wget -q "https://storage.googleapis.com/kubernetes-release/release/v1.21.0/bin/linux/amd64/kubectl"
    chmod +x kubectl
    mv kubectl /usr/local/bin/
  else
    echo "kubectl already installed" | tee /tmp/step-bootstrap-controlplane
  fi
  set +euo pipefail
)}

function generate_k8s_config_files {(
  set -euo pipefail
  echo "setting up env" | tee /tmp/step-bootstrap-controlplane
  mkdir -p /usr/k8s-files/others
  echo "generating encryption config file" | tee /tmp/step-bootstrap-controlplane
  ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)
  cat > /usr/k8s-files/others/encryption-config.yaml << EOF
---
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: $${ENCRYPTION_KEY}
      - identity: {}
EOF
  echo "generating kube scheduler config file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/others/kube-scheduler.yaml << EOF
---
apiVersion: kubescheduler.config.k8s.io/v1beta1
kind: KubeSchedulerConfiguration
clientConnection:
  kubeconfig: "/var/lib/kubernetes/kube-scheduler.kubeconfig"
leaderElection:
  leaderElect: true
EOF
  echo "generating apiserver-clusterrole.yaml file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/others/apiserver-clusterrole.yaml << EOF
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  labels:
    kubernetes.io/bootstrapping: rbac-defaults
  name: system:kube-apiserver-to-kubelet
rules:
  - apiGroups:
      - ""
    resources:
      - nodes/proxy
      - nodes/stats
      - nodes/log
      - nodes/spec
      - nodes/metrics
    verbs:
      - "*"
EOF
  echo "generating apiserver-clusterrolebinding.yaml file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/others/apiserver-clusterrolebinding.yaml << EOF
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: system:kube-apiserver
  namespace: ""
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:kube-apiserver-to-kubelet
subjects:
  - apiGroup: rbac.authorization.k8s.io
    kind: User
    name: kubernetes
EOF
  set +euo pipefail
)}

function generate_service_files {(
  set -euo pipefail
  echo "creating dir for service files" | tee /tmp/step-bootstrap-controlplane
  mkdir -p /usr/k8s-files/service-files
  echo "getting controllers private ips" | tee /tmp/step-bootstrap-controlplane
  INITIAL_CLUSTER=$(get_controllers_internal_ips_for_etcd_service)
  echo "getting etcd servers private ips" | tee /tmp/step-bootstrap-controlplane
  ETCD_SERVERS=$(get_controllers_etcd_server_ips)
  ETCD_SERVER_NAME=$(hostname -s)
  echo "etcd server name: $ETCD_SERVER_NAME"
  echo "initial cluster config: $INITIAL_CLUSTER"
  echo "etcd servers: $ETCD_SERVERS"
  echo "creating etcd.service file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/service-files/etcd.service << EOF 
[Unit]
Description=etcd
Documentation=https://github.com/coreos

[Service]
Type=notify
ExecStart=/usr/local/bin/etcd \\
  --name $${ETCD_SERVER_NAME} \\
  --cert-file=/etc/etcd/kubernetes.pem \\
  --key-file=/etc/etcd/kubernetes-key.pem \\
  --peer-cert-file=/etc/etcd/kubernetes.pem \\
  --peer-key-file=/etc/etcd/kubernetes-key.pem \\
  --trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-trusted-ca-file=/etc/etcd/ca.pem \\
  --peer-client-cert-auth \\
  --client-cert-auth \\
  --initial-advertise-peer-urls https://$${INTERNAL_IP}:2380 \\
  --listen-peer-urls https://$${INTERNAL_IP}:2380 \\
  --listen-client-urls https://$${INTERNAL_IP}:2379,https://127.0.0.1:2379 \\
  --advertise-client-urls https://$${INTERNAL_IP}:2379 \\
  --initial-cluster-token etcd-cluster-0 \\
  --initial-cluster $${INITIAL_CLUSTER} \\
  --initial-cluster-state new \\
  --data-dir=/var/lib/etcd
Restart=on-failure
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOF
  echo "creating kube-apiserver.service file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/service-files/kube-apiserver.service << EOF
[Unit]
Description=Kubernetes API Server
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-apiserver \\
  --advertise-address=$${INTERNAL_IP} \\
  --allow-privileged=true \\
  --apiserver-count=3 \\
  --audit-log-maxage=30 \\
  --audit-log-maxbackup=3 \\
  --audit-log-maxsize=100 \\
  --audit-log-path=/var/log/audit.log \\
  --authorization-mode=Node,RBAC \\
  --bind-address=0.0.0.0 \\
  --client-ca-file=/var/lib/kubernetes/ca.pem \\
  --enable-admission-plugins=NamespaceLifecycle,NodeRestriction,LimitRanger,ServiceAccount,DefaultStorageClass,ResourceQuota \\
  --etcd-cafile=/var/lib/kubernetes/ca.pem \\
  --etcd-certfile=/var/lib/kubernetes/kubernetes.pem \\
  --etcd-keyfile=/var/lib/kubernetes/kubernetes-key.pem \\
  --etcd-servers=$${ETCD_SERVERS} \\
  --event-ttl=1h \\
  --encryption-provider-config=/var/lib/kubernetes/encryption-config.yaml \\
  --kubelet-certificate-authority=/var/lib/kubernetes/ca.pem \\
  --kubelet-client-certificate=/var/lib/kubernetes/kubernetes.pem \\
  --kubelet-client-key=/var/lib/kubernetes/kubernetes-key.pem \\
  --runtime-config='api/all=true' \\
  --service-account-key-file=/var/lib/kubernetes/service-account.pem \\
  --service-account-signing-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-account-issuer=https://$${LB_PUBLIC_IP}:443 \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --service-node-port-range=30000-32767 \\
  --tls-cert-file=/var/lib/kubernetes/kubernetes.pem \\
  --tls-private-key-file=/var/lib/kubernetes/kubernetes-key.pem \\
  --v=2
Restart=on-failure
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOF
  echo "creating kube-controller-manager.service file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/service-files/kube-controller-manager.service << EOF
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-controller-manager \\
  --bind-address=0.0.0.0 \\
  --allocate-node-cidrs \\
  --cluster-cidr=$${CLUSTER_CIDR} \\
  --cluster-name=${cluster_name} \\
  --cluster-signing-cert-file=/var/lib/kubernetes/ca.pem \\
  --cluster-signing-key-file=/var/lib/kubernetes/ca-key.pem \\
  --kubeconfig=/var/lib/kubernetes/kube-controller-manager.kubeconfig \\
  --leader-elect=true \\
  --root-ca-file=/var/lib/kubernetes/ca.pem \\
  --service-account-private-key-file=/var/lib/kubernetes/service-account-key.pem \\
  --service-cluster-ip-range=10.32.0.0/24 \\
  --use-service-account-credentials=true \\
  --v=2
Restart=on-failure
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOF
  echo "creating kube-scheduler.service file" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/service-files/kube-scheduler.service << EOF
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
ExecStart=/usr/local/bin/kube-scheduler \\
  --config=/etc/kubernetes/config/kube-scheduler.yaml \\
  --v=2
Restart=on-failure
RestartSec=5
User=root

[Install]
WantedBy=multi-user.target
EOF
  set +euo pipefail
)}

function start_k8s_controller_services {(
  set -euo pipefail
  echo "reloading daemon" | tee /tmp/step-bootstrap-controlplane
  systemctl daemon-reload
  echo "starting kube-apiserver" | tee /tmp/step-bootstrap-controlplane
  systemctl enable kube-apiserver
  systemctl start kube-apiserver
  echo "starting kube-controller-manager" | tee /tmp/step-bootstrap-controlplane
  systemctl enable kube-controller-manager
  systemctl start kube-controller-manager
  echo "starting kube-scheduler" | tee /tmp/step-bootstrap-controlplane
  systemctl enable kube-scheduler
  systemctl start kube-scheduler
  set +euo pipefail
)}

function configure_healthchecks {(
  set -euo pipefail
  echo "setting up env" | tee /tmp/step-bootstrap-controlplane
  mkdir -p /usr/k8s-files/nginx
  echo "creating kubernetes.default.svc.cluster.local" | tee /tmp/step-bootstrap-controlplane
  cat > /usr/k8s-files/nginx/kubernetes.default.svc.cluster.local << EOF
server {
  listen      80;
  server_name kubernetes.default.svc.cluster.local;

  location /healthz {
     proxy_pass                    https://127.0.0.1:6443/healthz;
     proxy_ssl_trusted_certificate /var/lib/kubernetes/ca.pem;
  }
}
EOF
  cp /usr/k8s-files/nginx/kubernetes.default.svc.cluster.local /etc/nginx/sites-available/
  ln -s /etc/nginx/sites-available/kubernetes.default.svc.cluster.local /etc/nginx/sites-enabled/
  echo "starting ngnix" | tee /tmp/step-bootstrap-controlplane
  systemctl enable nginx
  systemctl restart nginx
  set +euo pipefail
)}

function verify_controller {(
  set -euo pipefail
  echo "verifying kubectl" | tee /tmp/step-bootstrap-controlplane
  kubectl cluster-info --kubeconfig /usr/k8s-files/kubeconfig/admin.kubeconfig
  echo "verifying ngnix" | tee /tmp/step-bootstrap-controlplane
  curl -H "Host: kubernetes.default.svc.cluster.local" -i http://127.0.0.1/healthz
  set +euo pipefail
)}

function setup_apiserver_rbac {(
  set -euo pipefail
  echo "setting up apiserver rbac via kubectl" | tee /tmp/step-bootstrap-controlplane
  kubectl apply -f /usr/k8s-files/others/apiserver-clusterrole.yaml --kubeconfig /usr/k8s-files/kubeconfig/admin.kubeconfig
  kubectl apply -f /usr/k8s-files/others/apiserver-clusterrolebinding.yaml --kubeconfig /usr/k8s-files/kubeconfig/admin.kubeconfig
  set +euo pipefail
)}


#####
# SCRIPT
#####

# if there is an error
# call a lambda
# this lambda needs the error message and the instance id
# lambda should terminate the instance as userdata failed to complete
FUNCTION_LIST="tag_instance wait_for_files get_pki_files get_kubeconfig_files generate_k8s_config_files generate_service_files bootstrap_etcd_cluster bootstrap_k8s_controller configure_healthchecks verify_controller setup_apiserver_rbac"
FUNCTION_LIST_ARRAY=($FUNCTION_LIST)

for function in "$${FUNCTION_LIST_ARRAY[@]}"
do
  $function
  ERROR="$?"
  if [[ "$ERROR" -ne 0 ]]
  then
    # call lambda error function
    curl_slack
    # shutdown now
    exit 1
  fi
done


#####
# CLEANUP
#####

rm /tmp/step-bootstrap-controlplane
