#####
resource "aws_iam_role" "kubeconfig" {
  
  name = var.kubeconfig_role_name
  tags = var.common_tags
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "STS1"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
  
  inline_policy {
    name = "${var.kubeconfig_role_name}-${var.bucket_name}-s3"
    policy = jsonencode(
      {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Sid": "S3P1",
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${var.bucket_name}"
          },
          {
            "Sid": "S3P2",
            "Action": "s3:*",
            "Effect": "Allow",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*"
          }
        ]
      }
    )
  }
  
  inline_policy {
    name = "${var.kubeconfig_role_name}-${var.bucket_name}-logs"
    policy = jsonencode(
      {
        "Version": "2012-10-17",
        "Statement": [
          {
            "Effect": "Allow",
            "Action": [
              "logs:CreateLogGroup",
              "logs:CreateLogStream",
              "logs:PutLogEvents"
            ],
            "Resource": "*"
            "Sid": "LogsP1",
          }
        ]
      }
    )
  }
  
}

# create a lambda function that provisions the necessary pki infrastructure
data "aws_ecr_repository" "kubeconfig" {
  name = var.kubeconfig_ecr_repo_name
}

resource "aws_lambda_function" "kubeconfig" {
  depends_on = [aws_s3_bucket.cluster]
  function_name = var.kubeconfig_function_name
  role = aws_iam_role.kubeconfig.arn
  package_type = "Image"
  image_uri = "${data.aws_ecr_repository.kubeconfig.repository_url}:${var.kubeconfig_ecr_image_tag}"
  timeout = 180
  memory_size = 256
  tags = var.common_tags
}

# provision ca and the admin client certificates
resource "null_resource" "kubeconfig" {

  # "lb_public_ip": "${local.lb_ip}",
        
  triggers = {
    always_run = var.run_kubeconfig_lambda == "always" ? "${timestamp()}" : ""
  }

  depends_on = [
    aws_lambda_function.kubeconfig,
    aws_iam_role.kubeconfig,
    null_resource.pki,
  ]

  provisioner "local-exec" {
  
    command = <<EOT
    sleep 30 && \
    aws lambda invoke \
      --function-name ${var.kubeconfig_function_name} \
      --invocation-type Event \
      --cli-binary-format raw-in-base64-out \
      --payload '{
        "bucket": "${var.bucket_name}",
        "pki_files_dir": "${var.pki_files_dir}",
        "kubeconfig_files_dir": "${var.kubeconfig_files_dir}",
        "lb_public_ip": "${aws_lb.main.dns_name}",
        "number_of_pki_files": "${var.number_of_pki_files}",
        "cluster_name": "${var.cluster_name}"
      }' \
      outfile
    EOT
    
    interpreter = ["/bin/bash", "-c"]
    
  }
  
}