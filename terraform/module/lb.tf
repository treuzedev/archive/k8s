resource "aws_lb" "main" {

  name = var.lb.name
  internal = false
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = true
  subnets = [for subnet in var.lb.subnets : aws_subnet.main[subnet].id]
  tags = var.common_tags
  
}

data "aws_network_interface" "lb" {

  for_each = toset(var.lb.subnets)

  filter {
    name   = "description"
    values = ["ELB ${aws_lb.main.arn_suffix}"]
  }

  filter {
    name   = "subnet-id"
    values = [aws_subnet.main[each.key].id]
  }
  
}

resource "aws_security_group" "lb" {

  name = "${var.cluster_name}-nlb"
  description = "A SG that allows a NLB access to K8s Control Plane nodes."
  vpc_id = aws_vpc.main.id
  tags = var.common_tags
  
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = formatlist("%s/32", [for eni in data.aws_network_interface.lb : eni.private_ip])
    description = "Allow connection from NLB network cards in all defined AZs, using all ports and protocols."
  }
  
}

resource "aws_lb_target_group" "control_plane" {

  name = "${var.cluster_name}-tg-controlplane"
  port = var.lb.control_plane_tg.port
  protocol = var.lb.control_plane_tg.protocol
  target_type = var.lb.control_plane_tg.target_type
  vpc_id = aws_vpc.main.id
  tags = var.common_tags
  
  health_check {
    enabled = var.lb.control_plane_tg.health_check.enabled
    healthy_threshold = var.lb.control_plane_tg.health_check.healthy_threshold
    unhealthy_threshold = var.lb.control_plane_tg.health_check.unhealthy_threshold
    interval = var.lb.control_plane_tg.health_check.interval
    path = var.lb.control_plane_tg.health_check.path
    port = var.lb.control_plane_tg.health_check.port
  }
  
}

resource "aws_lb_target_group_attachment" "control_plane" {
  count = 3
  target_group_arn = aws_lb_target_group.control_plane.arn
  target_id = aws_instance.control_plane[count.index].private_ip
}

resource "aws_lb_listener" "control_plane" {

  load_balancer_arn = aws_lb.main.arn
  port = 443
  protocol = "TCP"
  tags = var.common_tags
  
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.control_plane.arn
  }
  
}