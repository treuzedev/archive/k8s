#####
# GENERAL VARS
#####

#####
variable "cluster_name" {
  description = "Cluster name. Defaults to treuze-prod."
  default = "treuze-prod"
}

#####
variable "cluster_cidr" {
  description = "Cluster name. Defaults to '10.200.0.0/16'."
  default = "10.200.0.0/16"
}

#####
variable "key_pair_name" {
  description = "AWS Key Pair Name. Defaults to treuze-k8s-nodes."
  default = "treuze-k8s-nodes"
}

#####
variable "key_pair_content" {
  description = "Key pair to be used for SSH access to K8s nodes. Has default."
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD4/slmsDhttPW0YCvu59HU/tsuISn+4X9T7UFHBpUJjoatBPjUtLVmxH4FT57YxP6rmpX/UBUyc5lsxuQJK+E0aK/AdDoRvZ1QVTFjdNZXzf8CmsyZjCD63TTMOgBaAYjvxuVT41Xn1H3wZBpnE16Xxs19tFli0HnCRCvYIiD3GEsGdHpgm/KhLrOUfqqA0JSIEaW0ngAFpoIsYHeKFUcDynR0SQjs9LH+6qBtfTeYS0rWNvM+B0du+fsmnU4JofTj75y5+l2qgOjsA72aAqfCbrYOiDQGlbaa/IZAWyRiMYUeiQFi5aPDvJIXNUiCoG1cB1qLJgGtCWLK5UDqXWYd cloud9"
}

#####
variable "k8s_tags" {

  description = <<EOT
    Treuze K8s common tags.\n
    Defaults to:
    {
      "Environment" : "Production",
      "Owner" : "EMU",
      "App" : "Treuze-K8s",
    }
    EOT

  default = {
    "Environment" : "Production",
    "Owner" : "EMU",
    "App" : "Treuze-K8s",
  }

}

#####
variable "control_plane_app_tag" {
  description = "App tag value passed to control plane nodes to allow them to discover other control plane nodes. Defaults to 'Treuze-K8s'."
  default = "Treuze-K8s"
}

#####
variable "control_plane_component_tag" {
  description = "Component tag value passed to control plane nodes to allow them to discover other control plane nodes. Also used to tag instances apart from the common tags. Defaults to 'ControlPlane'."
  default = "ControlPlane"
}

#####
variable "control_plane_environment_tag" {
  description = "Environment tag value passed to control plane nodes to allow them to discover other control plane nodes. Defaults to 'Production'."
  default = "Production"
}

#####
variable "slack_webhook_url" {
  description = "Slack endpoint for instances to send error messages. Defaults to: https://hooks.slack.com/services/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."
  default = "https://hooks.slack.com/services/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"
}


#####
# LB
#####

#####
variable "lb" {
  description = <<EOT
    A map defining the LB that fronts the cluster. Defaults to:
    EOT
  default = {
    "name": "treuze-k8s-lb",
    "subnets": [
      "alpha-public",
      "bravo-public",
      "charlie-public",
    ],
    "control_plane_tg": {
      "port": 6443,
      "protocol": "TCP",
      "target_type": "ip",
      "health_check": {
        "enabled": true,
        "healthy_threshold": 3,
        "unhealthy_threshold": 3,
        "interval": 30,
        # "path": "kubernetes.default.svc.cluster.local/healthz",
        "path": "/healthz",
        "port": 80,
      }
    }
  }
}


#####
# VPC
#####

#####
variable "bastion" {
    description = <<EOT
      A map defining a bastion instance. Defaults to:
      
      EOT
    default = {
      "subnet": "bravo-public",
      "sgs": [
        "bastion",
        "bastion-access",
        "all-out",
      ],
      "instance_type": "t2.micro",
      "ami": "ami-03caf24deed650e2c",
      "extra_tags": {
        "Component": "Bastion",
        "Name": "bastion-treuze-k8s-prod",
      }
    }
}

#####
variable "vpc_cidr_block" {
  description = "Treuze K8s VPC CIDR block. Defaults to 10.1.0.0/16"
  default       = "10.1.0.0/16"
}

#####
variable "subnets" {

  description = <<EOT
    A map containing maps configuring the K8s VPC subnets.
    Each key is the name of the subnet.
    Each map should have three keys, namely, az, cidr and rt_name.
    Note that the rt_name must match one defined in the var vpc_rts.
    Defaults to:
    {
      "alpha-public" : {
        "az" : "eu-west-1a",
        "cidr" : "10.1.0.0/24",
        "rt_name" : "public",
      },
      "bravo-public" : {
        "az" : "eu-west-1b",
        "cidr" : "10.1.1.0/24",
        "rt_name" : "public",
      },
      "charlie-public" : {
        "az" : "eu-west-1c",
        "cidr" : "10.1.2.0/24",
        "rt_name" : "public",
      },
      "alpha-private" : {
        "az" : "eu-west-1a",
        "cidr" : "10.1.3.0/24",
        "rt_name" : "private",
      },
      "bravo-private" : {
        "az" : "eu-west-1b",
        "cidr" : "10.1.4.0/24",
        "rt_name" : "private",
      },
      "charlie-private" : {
        "az" : "eu-west-1c",
        "cidr" : "10.1.5.0/24",
        "rt_name" : "private",
      },
    }
    EOT

  default = {
    "alpha-public" : {
      "az" : "eu-west-1a",
      "cidr" : "10.1.0.0/24",
      "rt_name" : "public",
    },
    "bravo-public" : {
      "az" : "eu-west-1b",
      "cidr" : "10.1.1.0/24",
      "rt_name" : "public",
    },
    "charlie-public" : {
      "az" : "eu-west-1c",
      "cidr" : "10.1.2.0/24",
      "rt_name" : "public",
    },
    "alpha-private" : {
      "az" : "eu-west-1a",
      "cidr" : "10.1.3.0/24",
      "rt_name" : "private",
    },
    "bravo-private" : {
      "az" : "eu-west-1b",
      "cidr" : "10.1.4.0/24",
      "rt_name" : "private",
    },
    "charlie-private" : {
      "az" : "eu-west-1c",
      "cidr" : "10.1.5.0/24",
      "rt_name" : "private",
    },
  }

}

#####
variable "rts" {

  description = <<EOT
    Main vpc route tables. See default for more info.
    A map containing maps configuring each route table.
    The key is the name of the route table.
    Defaults to:
    {
      "public" : {
        "routes" : [
          {
            "cidr" : "0.0.0.0/0",
            "type" : "igw",
          }
        ]
      },
      "private" : {
        "routes" : [
          {
            "cidr" : "0.0.0.0/0",
            "type" : "nat",
            "subnet": "alpha-public",
          }
        ]
      },
    }
    EOT

  default = {
    "public" : {
      "routes" : [
        {
          "cidr" : "0.0.0.0/0",
          "type" : "igw",
        }
      ]
    },
    "private" : {
      "routes" : [
        {
          "cidr" : "0.0.0.0/0",
          "type" : "nat",
          "subnet": "alpha-public",
        }
      ]
    },
  }

}

#####
variable "nats" {
  
  description = <<EOT
  A list of strings, where each string corresponds to the name of a subnet.
  Defaults to:
  [
    "alpha-public",
  ]
  EOT
  
  default = [
    "alpha-public",
  ]
  
}

#####
variable "ngw_subnet" {
  description = "The name of the subnet where the K8s NGW lives. Defaults to 'alpha-public'."
  default = "alpha-public"
}

#####
variable "sgs" {

  description = <<EOT
    A map containing maps configuring a security group.
    Defaults to:
    EOT

  default = {
    "nodes" : {
      "name" : "k8s-nodes",
      "description" : "Allow all communication between K8s nodes",
      "ingress" : [
        {
          "from_port" : 0,
          "to_port" : 0,
          "protocol" : -1,
          "self": true,
          "description" : "Allow all incoming traffic from this SG. Used for comms between K8s nodes.",
        },
      ],
    },
    "bastion" : {
      "name" : "bastion",
      "description" : "Allow all exterior access via SSH.",
      "ingress" : [
        {
          "from_port" : 22,
          "to_port" : 22,
          "protocol" : "tcp",
          "cidr_blocks": [
            "0.0.0.0/0",
          ],
          "self": false,
          "description" : "Allow all incoming exterior traffic via SSH port 22.",
        },
      ],
    },
    "bastion-access" : {
      "name" : "bastion-access",
      "description" : "Allow all communication between K8s nodes and a bastion instance",
      "ingress" : [
        {
          "from_port" : 0,
          "to_port" : 0,
          "protocol" : -1,
          "self": true,
          "description" : "Allow all incoming traffic from this SG. Used for comms between a bastion and K8s nodes.",
        },
      ],
    },
    "all-out" : {
      "name" : "k8s-all_out",
      "description" : "Allow K8s nodes to send traffic everywhere.",
      "egress" : [
        {
          "from_port" : 0,
          "to_port" : 0,
          "protocol" : -1,
          "cidr_blocks": [
            "0.0.0.0/0"
          ],
          "description" : "Allow all traffic out.",
        },
      ],
    },
  }

}


#####
# NODES
#####

#####
variable "control_plane" {
  
  description = <<EOT
  A map configuring the control plane instances. Defaults to:
  EOT
  
  default = {
    "name": "control_plane",
    "image_id": "ami-03caf24deed650e2c",
    "instance_type": "t2.medium",
    "subnets": [
      "alpha-private",
      "bravo-private",
      "charlie-private",
    ],
    "sgs": [
      "nodes",
      "all-out",
      "bastion-access",
    ]
    "volume_size": 8,
  }
  
}

#####
variable "cluster_runtime" {
  
  description = <<EOT
  A map configuring the cluster runtime instances. Defaults to:
  EOT
  
  default = {
    "name": "cluster_runtime",
    "image_id": "ami-03caf24deed650e2c",
    "instance_type": "t2.medium",
    "worker_type": "clusterruntime"
    "subnets": [
      "alpha-private",
      "bravo-private",
      "charlie-private",
    ],
    "sgs": [
      "nodes",
      "all-out",
      "bastion-access",
    ]
    "volume_size": 8,
    "max_size": 3,
    "min_size": 1,
    "desired_capacity": 1
    "tags": [
      {
        "key": "App",
        "value": "Treuze-K8s",
        "propagate_at_launch": true,
      },
      {
        "key": "Owner",
        "value": "EMU",
        "propagate_at_launch": true,
      },
      {
        "key": "Environment",
        "value": "Production",
        "propagate_at_launch": true,
      },
      {
        "key": "Node",
        "value": "cluster_runtime",
        "propagate_at_launch": true,
      },
    ]
  }
  
}


#####
# PKI
#####

#####
variable "pki_role_name" {
  description = "The name of the Lambda execution role that provisions the necessary PKI infrastructure. Defaults to lambdaPkiExecutionRole."
  default = "lambdaPkiExecutionRole"
}

#####
variable "bucket_name" {
  description = "The bucket name to store cluster files. Defaults to treuze-k8s."
  default = "treuze-k8s"
}

#####
variable "pki_files_dir" {
  description = "The bucket dir name to store PKI infrastructure files. Defaults to pki."
  default = "pki"
}

#####
variable "number_of_pki_files" {
  description = "The expected number of PKI files in the bucket. Default to '30', a string."
  default = "30"
}

#####
variable "workers_files_dir" {
  description = "The bucket dir name to store worker nodes files. Defaults to 'workers'."
  default = "workers"
}

#####
variable "number_of_kubeconfig_files" {
  description = "The expected number of kubeconfig files in the bucket. Default to '7', a string."
  default = "7"
}

#####
variable "number_of_userdata_files" {
  description = "The expected number of userdata files in the bucket. Default to '3', a string."
  default = "3"
}

#####
variable "pki_function_name" {
  description = "The name of the Lambda function that provisions the PKI infrastructure. Defaults to treuze-k8s-pki."
  default = "treuze-k8s-pki"
}

#####
variable "pki_ecr_repo_name" {
  description = "The name of the ECR repo holding the PKI cfssl image. Defaults to cfssl-runtime."
  default = "cfssl-runtime"
}

#####
variable "pki_ecr_image_tag" {
  description = "The tag of the PKI cfssl image. Defaults to 0.1.1."
  default = "0.1.2"
}

#####
variable "certs" {

  description = <<EOT
  A map configuring the certificates provisioning. Defaults to:
  {
    "org": "Kubernetes",
    "country": "Ireland",
    "locality": "Dublin"
  }
  EOT
  
  default = {
    "org": "Kubernetes",
    "country": "Ireland",
    "locality": "Dublin"
  }
  
}

#####
# KUBECONFIG
#####

#####
variable "kubeconfig_role_name" {
  description = "The name of the Lambda execution role that provisions the necessary kubeconfig files. Defaults to kubeconfigExecutionRole."
  default = "kubeconfigExecutionRole"
}

#####
variable "kubeconfig_files_dir" {
  description = "The bucket dir name to store kubeconfig files. Defaults to kubeconfig."
  default = "kubeconfig"
}

#####
variable "kubeconfig_function_name" {
  description = "The name of the Lambda function that provisions the kubeconfig files. Defaults to treuze-k8s-kubeconfig."
  default = "treuze-k8s-kubeconfig"
}

#####
variable "kubeconfig_ecr_repo_name" {
  description = "The name of the ECR repo holding the kubectl image layer. Defaults to kubectl-runtime."
  default = "kubectl-runtime"
}

#####
variable "kubeconfig_ecr_image_tag" {
  description = "The tag of the kubectl layer image. Defaults to '0.1.2'."
  default = "0.1.5"
}

#####
variable "run_pki_lambda" {
  description = "A string that allows the PKI resources provision by the corresponding lambda to be recreated each time terraform apply is ran. This only happens if the value is 'always'. Any other value only runs lambda the first time the underlying 'ull_resource' is created. Default to 'once'."
  default = "once"
}


#####
variable "run_kubeconfig_lambda" {
  description = "A string that allows the kubeconfig files provision by the corresponding lambda to be recreated each time terraform apply is ran. This only happens if the value is 'always'. Any other value only runs lambda the first time the underlying 'ull_resource' is created. Defaults to 'once'."
  default = "once"
}

